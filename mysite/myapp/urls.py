
from django.contrib import admin
from django.urls import path
from django.conf.urls import include, url
from myapp.views import hello,viewArticle,user,edit,update,show,destroy,task,show_task,update_task,destroy_task,edit_task,login
urlpatterns = [
    path('admin/', admin.site.urls),
    url('^hello/', hello, name='hello'),
    # url(r'^article/(\d+)/', 'viewArticle', name='article'),
    path('article/<int:articleId>/', viewArticle),
    path('article/<int:month>/<int:year>', viewArticle),
    path('admin/', admin.site.urls),
    path('user', user),
    path('show', show),
    path('edit/<int:id>', edit),
    path('update/<int:id>', update),
    path('delete/<int:id>', destroy),
    path('task', task),
    path('show_task', show_task),
    path('edit_task/<int:id>', edit_task),
    path('update_task/<int:id>', update_task),
    path('delete_task/<int:id>', destroy_task),
    path('login', login),
]

