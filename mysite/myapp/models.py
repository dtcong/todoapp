from django.db import models
from datetime import datetime
# Create your models here.
STATUS_CHOICES= [
    (0, 'Chưa hoàn thành'),
    (1, 'Hoàn thành'),
    ]
class Task(models.Model):

   Name = models.CharField(max_length = 50)
   Content = models.CharField(max_length = 50)
   StartTime = models.DateTimeField(default=datetime.now, blank=True)
   EndTime = models.DateTimeField(default=datetime.now, blank=True)
   CreatedDate = models.DateTimeField(default=datetime.now, blank=True)
   # Status = models.IntegerField()
   Status=models.IntegerField(choices=STATUS_CHOICES, default=0)
   UserId=models.ForeignKey('User', default = 1,on_delete=models.CASCADE,related_name='user_in_charge')
   CreatedUser = models.ForeignKey('User', default=1, on_delete=models.CASCADE,related_name='created_user')

   class Meta:
      db_table = "task"


class User(models.Model):
    Username = models.CharField(max_length=30)
    Password = models.CharField(max_length=30, default='')
    def __str__(self):
        return self.Username


class Meta:
    db_table = "user"