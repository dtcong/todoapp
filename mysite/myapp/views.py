from django.shortcuts import render
from django.http import HttpResponse
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait

from myapp.models import Task
import datetime
from django.shortcuts import render, redirect
# Create your views here.
from myapp.forms import UserForm,TaskForm,LoginForm
from myapp.models import User,Task,STATUS_CHOICES
from selenium import webdriver
from selenium.webdriver.support.select import Select
from selenium.webdriver.support import expected_conditions as ec
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from django.contrib import messages
def user(request):
    if request.method == "POST":
        form = UserForm(request.POST)
        if form.is_valid():
            try:
                form.save()
                return redirect('/myapp/show')
            except:
                pass
    else:
        form = UserForm()
    return render(request,'index.html',{'form':form})
def show(request):
    users = User.objects.all()
    return render(request,"show.html",{'users':users})
def edit(request, id):
    user = User.objects.get(id=id)
    return render(request,'edit.html', {'user':user})
def update(request, id):
    user = User.objects.get(id=id)
    form = UserForm(request.POST, instance = user)
    if form.is_valid():
        form.save()
        return redirect("/myapp/show")
    return render(request, 'edit.html', {'user': user})
def destroy(request, id):
    employee = User.objects.get(id=id)
    employee.delete()
    return redirect("/myapp/show")

def task(request):
    un = request.session['logUN'];
    usr = User.objects.get(Username=un)
    if request.method == "POST":

        form = TaskForm(request.POST,initial={'CreatedUser': usr})
        if form.is_valid():
            try:
                form.save()
                return redirect('/myapp/show_task')
            except Exception as ex:
                templ = "An exception of type {0} occurred. Arguments:\n{1!r}"
                message = templ.format(type(ex).__name__, ex.args)
                print (message)
    else:
        form = TaskForm(initial={'CreatedUser': usr})
    users = User.objects.all()
    return render(request,'index_task.html',{'form':form,'users':users})

def show_task(request):
    tasks = Task.objects.all()

    return render(request,"show_task.html",{'tasks':tasks})
def edit_task(request, id):
    task = Task.objects.get(id=id)
    users = User.objects.all()

    return render(request,'edit_task.html', {'task':task,"users":users})
def update_task(request, id):
    task = Task.objects.get(id=id)
    users = User.objects.all()
    if 'FinishTask' in request.POST:
        task.Status = 1
        task.save()
        return redirect("/myapp/show_task")
    form = TaskForm(request.POST, instance = task)

    if form.is_valid():
        form.save()
        return redirect("/myapp/show_task")
    return render(request, 'edit_task.html', {'task': task,"users":users})
def destroy_task(request, id):
    task = Task.objects.get(id=id)
    task.delete()
    return redirect("/myapp/show_task")


def login(request):
    username = "not logged in"

    if request.method == "POST":
        # Get the posted form
        form = LoginForm(request.POST)
        print(form.errors)
        if form.is_valid():
            username = form.cleaned_data['user']
            password = form.cleaned_data['password']
            try:
                user = User.objects.get(Username=form.cleaned_data['user'], Password=form.cleaned_data['password'])
            except Exception as e:
                messages.error(request, 'username or password not correct')
                return render(request, "login.html", {'form': form})
            request.session['logUN'] = form.cleaned_data['user']
            request.session['logPS'] = form.cleaned_data['password']
            return redirect("/myapp/show_task")

    else:
        form = LoginForm()

    return render(request, "login.html", {'form': form})




def hello(request):
   text = """<h1>welcome to my app !</h1>"""
   return HttpResponse(text)

def hello(request):
   today = datetime.datetime.now().date()
   daysOfWeek = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']

   return render(request,'hello.html', {'today' : today,"days_of_week" : daysOfWeek})
def viewArticle(request, articleId):
   text = "Displaying article Number : %s"%articleId
   return HttpResponse(text)
def viewArticle(request, month, year):
   text = "Displaying articles of : %s/%s"%(year, month)
   return HttpResponse(text)