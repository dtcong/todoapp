from django import forms
from myapp.models import User,Task
from django.forms import formset_factory
from datetime import datetime
class UserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = "__all__"
STATUS_CHOICES= [
    (0, 'Chưa hoàn thành'),
    (1, 'Hoàn thành'),
    ]
class TaskForm(forms.ModelForm):
    class Meta:
        model = Task
        fields = ['id','Name','Content','StartTime','EndTime','CreatedDate','Status','UserId','CreatedUser']
        widgets = {'Status': forms.HiddenInput(),'CreatedUser': forms.HiddenInput()}

    def get_initial(self,User,Status):
        # call super if needed
        return {'CreatedUser': User,'Status':Status}


class LoginForm(forms.Form):
    user = forms.CharField(max_length=100)
    password = forms.CharField(widget=forms.PasswordInput())


        # fields = "__all__"
# class TaskForm(forms.Form):
#     Name = forms.CharField(max_length=50)
#     Content = forms.CharField(max_length=50)
#     StartTime = forms.DateTimeField(initial=datetime.now())
#     EndTime = forms.DateTimeField(initial=datetime.now())
#     # Status = forms.IntegerField()
#     Status = forms.IntegerField(choices=STATUS_CHOICES, default=0)
    # UserId = forms.ForeignKey('User', default=1, on_delete=models.CASCADE)

